https://use-the-index-luke.com/no-offset


Postgres commands
1) create db
``CREATE DATABASE lifeManagerDB;``

2)create user
```
CREATE USER dbadmin WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	VALID UNTIL '2025-04-03T11:50:38+05:30' 
	PASSWORD 'dbpass2077';
```

* list of databases ``\l``;
* list of users ``\du``;

3) Add an existing user to a Database
You can grant a user privileges to access a database.

``GRANT ALL PRIVILEGES ON DATABASE lifeManagerDB TO dbadmin;``

4) CREATE TABLE


```
CREATE TABLE table_name (
	field_name data_type constrain_name, 
	field_name data_type constrain_name
);
```
* table_name: Is the name of the table
* field_name: Is the name the column
* data_type: Is the variable type of the column
* constrain_name: Is optional. It defines constraints on the column.

Tables never have the same name as any existing table in the same schema.

Example: ``CREATE TABLE tutorials (id int, tutorial_name text);``

Use the parameter IF NOT EXISTS and you will get a notice instead of an error

-> ``CREATE TABLE IF NOT EXISTS tutorials (id int, tutorial_name text);``

Use command ``\d`` to check the list of relations (tables);


-----

5) Drop table
``DROP TABLE tutorials;`` (-> ``DROP TABLE IF EXISTS tutorials;``)

6) Select (source=``https://www.guru99.com/postgresql-select-distinct.html``)
``SELECT *  FROM tutorials;``
``SELECT *  FROM tutorials ORDER BY id;``
descement ``SELECT *  FROM tutorials ORDER BY id DESC;``