import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Task } from './task.model';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column({
        unique: true
    })
    public username: string;

    @Column()
    public password: string;

    @OneToMany(type => Task, photo => photo.user)
    tasks: Task[];
}
