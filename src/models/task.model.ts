import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { User } from './user.model';

@Entity()
export class Task {
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Column()
    public value: string;

    @ManyToOne(type => User, user => user.tasks)
    user: User;
}
