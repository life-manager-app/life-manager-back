import * as Koa from 'koa';
import { Connection } from 'typeorm';
import { User } from '../models/user.model';

const USER_ALREADY_EXISTS_ERROR =
    'duplicate key value violates unique constraint ';

export class UserControllerFactory {
    private repository = null;

    constructor(private connection: Connection) {
        this.repository = connection.getRepository(User);
    }

    create = async (ctx: Koa.BaseContext) => {
        const user = new User();

        user.username = ctx.request.body.username;
        user.password = ctx.request.body.password;

        try {
            ctx.status = 201;
            ctx.body = await this.connection.manager.save(user);
        } catch (error) {
            ctx.status = 400;

            // don't work
            if (error.message.startsWith(USER_ALREADY_EXISTS_ERROR)) {
                ctx.body = 'asdasdasd';
            }

            ctx.body = 'Create user error';
        }
    };

    login = async (ctx: Koa.BaseContext) => {
        const { username, password } = ctx.request.body;

        try {
            const {id} = await this.repository.findOne({
                where: { username, password }
            });

            ctx.status = 200;
            ctx.body = {id};
        } catch (error) {
            ctx.status = 400;
            ctx.body = 'Login user error';
        }
    };
}
