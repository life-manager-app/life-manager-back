import * as Koa from 'koa';
import { Connection } from 'typeorm';
import { cookiesNameId } from '../server';
import { User } from '../models/user.model';
import { Task } from '../models/task.model';

export class TaskControllerFactory {
    constructor(private connection: Connection) {}

    create = async (ctx: Koa.BaseContext) => {
        const id = this.getIdFromCookies(ctx);

        const userRepository = this.connection.getRepository(User);
        const user = await userRepository.findOne({ where: { id } });

        const task = new Task();

        task.value = ctx.request.body.data;
        task.user = user;

        await this.connection.manager.save(task);

        ctx.body = task;
    };

    retrieve = async (ctx: Koa.BaseContext) => {
        // const experimentRecordRepository = this.connection.getRepository(ExperimentRecord);
        // ctx.body = JSON.stringify(await experimentRecordRepository.find());

        ctx.body = 'try to retrieve task';
    };

    list = async (ctx: Koa.BaseContext) => {
        ctx.body = 'try to retrieve list tasks';
    };

    update = async (ctx: Koa.BaseContext) => {
        ctx.body = 'try to update tasks';
    };

    private getIdFromCookies(ctx: Koa.BaseContext): string {
        const id = ctx.cookies.get(cookiesNameId);

        return id;
    }
}
