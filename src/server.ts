import * as Koa from 'koa';
import * as Router from 'koa-router';
import * as bodyParser from 'koa-bodyparser';
import { createConnection } from 'typeorm';

import 'reflect-metadata'; // todo - нужно ли это?

import { getDatabaseConfig } from './serverConfigs';
import { TaskControllerFactory } from './controllers/task.controller';
import { UserControllerFactory } from './controllers/user.controller';

const app = new Koa();
const router = new Router();
const PORT = 3332;

export const cookiesNameId = 'limaUserId';

const options = getDatabaseConfig();
console.log(`DB OPTIONS: ${JSON.stringify(options)}`);

createConnection().then(async connection => {
    const taskController = new TaskControllerFactory(connection);
    const userController = new UserControllerFactory(connection);

    // middlewares
    app.use(bodyParser());
    app.use(async (ctx, next) => {
        console.log(`url: ${ctx.URL}`);

        // Pass the request to the next middleware function
        await next();
    });

    // todo - create middleware, which will redirect to "/api" prefix
    router.post('/api/user', userController.create);
    router.post('/api/login', userController.login);

    router.post('/api/task', taskController.create);
    router.get('/task', taskController.list);
    router.get('/api/task/id', taskController.retrieve);
    router.put('/task/id', taskController.update);

    router.get('/*', async ctx => {
        ctx.body = 'Hello World!';
    });

    app.use(router.routes());
    app.listen(PORT);

    console.log(`Server running on port ${PORT}`);
}).catch(error => console.log(`createConnection error ${error}`));
