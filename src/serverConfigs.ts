import { ConnectionOptions } from 'typeorm';

const options = {
    type: 'postgres',
    host: 'localhost',
    port: 5433,
    username: 'postgres',
    password: 'postgres',
    database: 'lifemanagerdb',
    synchronize: true,
    logging: false,
    entities: 'src/models/**/*.ts',
    migrations: ['src/migration/**/*.ts'],
    subscribers: ['src/subscriber/**/*.ts']
};

export function getDatabaseConfig(): ConnectionOptions {
    return options as any;
}
